from itertools import islice
import sys
import datetime as dt
import logging
import numpy as np
from scipy.io import loadmat
from scipy.io.matlab.mio5 import varmats_from_mat
from netCDF4 import Dataset, date2num


def mat_get_unique_variables_times(llista):
    """ Gets unique variables and times from the Matlab variable list """
    logging.info("Splitting Matlab variables to get unique values...")
    variables = []
    times = []
    for var in llista:
        trencat = var.split('_')
        if len(trencat) > 1 and not trencat[1].isnumeric():
            variables.append(var)
        elif len(trencat) > 1:
            variables.append(trencat[0])
            times.append(dt.datetime.strptime(trencat[1] + '_' + trencat[2], '%Y%m%d_%H%M%S'))
        else:
            variables.append(trencat[0])
    unq_vars = np.unique(variables)
    if len(unq_vars) == 0:
        logging.error("No variables found.")
    else:
        logging.info("%s variables found:", len(unq_vars))
        for var in unq_vars:
            logging.info(" - %s", var)
    unq_times = np.unique(times)
    if len(unq_times) == 0:
        logging.error("No time steps found.")
    else:
        logging.info("%s times found:", len(unq_times))
        for instant in unq_times:
            logging.info(" - %s", instant.strftime('%Y-%m-%d %H %M %S'))
    return unq_vars, unq_times


class Mat2Nc:
    """ Converts SWAN .mat to .nc, using unstructured grid """

    def read_grid(self, fitxer):
        """ Reads grid in ADCIRC format - fort.14 file """
        logging.info("Reading grid file...")
        salta_linies = 2  # jumps line 0 and 1, as they are headers
        with open(fitxer) as fit:
            for i, line in enumerate(fit):
                if i == 1:
                    num_cares, num_nodes = line.split()
                    break
        num_cares = int(num_cares)
        num_nodes = int(num_nodes)
        logging.info("Getting coordinates. This can take a while, please wait...")
        with open(fitxer) as fit:
            node_id = lon = lat = bath = []
            for line in islice(fit, salta_linies, salta_linies + int(num_nodes)):
                node_id, lon, lat, bath = line.split()
                self.lons = np.append(self.lons, float(lon))
                self.lats = np.append(self.lats, float(lat))
        logging.info("Getting grid faces. This can take a while, please wait...")
        cara1 = cara2 = cara3 = []
        with open(fitxer) as fit:
            face_id = sides = car1 = car2 = car3 = []
            for line in islice(fit, salta_linies + num_nodes, salta_linies + num_nodes + num_cares):
                face_id, sides, car1, car2, car3 = line.split()
                cara1 = np.append(cara1, int(car1))
                cara2 = np.append(cara2, int(car2))
                cara3 = np.append(cara3, int(car3))
                percent = int(face_id) / num_cares
                print(f"Face {face_id} of {num_cares} ({percent:.2%})\r", end='', flush=True)
        assert len(cara1) == len(cara2) == len(cara3)
        self.numcares = len(cara1)
        assert self.numcares == num_cares
        assert len(self.lons) == len(self.lats)
        i = 0
        logging.info("Joining faces into a single matrix...")
        while i < len(cara1):
            self.cares = np.vstack((self.cares, [cara1[i], cara2[i], cara3[i]]))
            i += 1
        logging.info('Done: %s nodes, %s triangles, dimension %s.' %
                     (num_nodes, num_cares, self.cares.ndim))

    def mat_get_variables(self):
        """ Get all Matlab variable names from a file """
        var_names = []
        dades = varmats_from_mat(open(self.fit_mat, 'rb'))
        for dada in dades:
            var_names.append(dada[0])
        logging.info("%s variables in total.", len(var_names))
        return var_names

    def mat_get_values(self, nom_variable):
        """ Get all values from a Matlab variable """
        dades = varmats_from_mat(open(self.fit_mat, 'rb'))
        for idx, dada in enumerate(dades):
            if dada[0] == nom_variable:
                vari = loadmat(dades[idx][1])
                return vari[nom_variable][0, :]
        return None

    def nc_add_swan_var(self, vname, vtype, vdim, std_name, lng_name, units, content):
        """ Write a variable to a NetCDF file."""
        if vname not in self.fit_nc.variables:
            var = self.fit_nc.createVariable(vname, vtype, vdim, fill_value=np.float64(-99999.0))
            var.standard_name = std_name
            var.long_name = lng_name
            var.location = 'node'
            var.mesh = 'swan_mesh'
            var.units = units
        else:
            var = self.fit_nc.variables[vname]
        if isinstance(vdim, tuple):
            var.coordinates = 'time longitude latitude'
            var[:, :] = content
        else:
            var.coordinates = 'longitude latitude'
            var[:] = content

    def nc_add_headers(self):
        """ Writes headers on a NetCDF file."""
        logging.info("Adding headers to NetCDF file...")
        self.fit_nc.description = 'SWAN Wave model output'
        self.fit_nc.history = 'File generated on: %s' % dt.datetime.now()
        self.fit_nc.Conventions = 'CF-1.5, UGRID-1.0'

    def nc_add_dimensions(self):
        """ Writes dimensions on a NetCDF file."""
        logging.info("Adding dimensions to NetCDF file...")
        self.fit_nc.createDimension('node', len(self.lons))
        self.fit_nc.createDimension('nele', self.numcares)
        self.fit_nc.createDimension('time', None)
        self.fit_nc.createDimension('nvertex', np.int32(3))
        self.fit_nc.createDimension('single', np.int32(1))

    def nc_create_file(self):
        """ Creates a new NetCDF file and writes headers and dimensions."""
        logging.info("Creating NetCDF file...")
        self.fit_nc = Dataset(self.fitxer_desti, 'w')
        self.nc_add_headers()
        self.nc_add_dimensions()

    def nc_write_mesh_data(self):
        """ Write mesh data on NetCDF file."""
        logging.info("Writing mesh data to NetCDF file...")
        node_lon = self.fit_nc.createVariable('longitude', np.float64, ('node',))
        node_lon.long_name = 'Longitude'
        node_lon.standard_name = 'longitude'
        node_lon.units = 'degrees_east'
        node_lon[:] = self.lons

        node_lat = self.fit_nc.createVariable('latitude', np.float64, ('node',))
        node_lat.long_name = 'Latitude'
        node_lat.standard_name = 'latitude'
        node_lat.units = 'degrees_north'
        node_lat[:] = self.lats

        element = self.fit_nc.createVariable('element', np.int32, ('nele', 'nvertex'))
        element.long_name = 'Mesh element'
        element.cf_role = 'face_node_connectivity'
        element.start_index = np.int32(1)
        element[:] = self.cares

        mesh = self.fit_nc.createVariable('swan_mesh', np.int32, 'single')
        mesh.long_name = 'Mesh topology'
        mesh.cf_role = 'mesh_topology'
        mesh.topology_dimension = np.int32(2)
        mesh.node_coordinates = 'longitude latitude'
        mesh.face_node_connectivity = 'element'

    def nc_write_vars(self):
        """ Get variables and times from Matlab, and writes on NetCDF file."""
        var_list = self.mat_get_variables()
        self.mat_vars, self.mat_times = mat_get_unique_variables_times(var_list)

        logging.info("Writing variables to NetCDF file...")
        if self.mat_times.size > 0:
            times = self.fit_nc.createVariable('time', np.float32, ('time',))
            times.long_name = 'Model time'
            times.standard_name = 'time'
            times.units = 'seconds since 1970-01-01T00:00:00+00:00'
            times.calendar = 'gregorian'
            times[0:len(self.mat_times)] = date2num(self.mat_times, times.units, times.calendar)

        # Variables without time steps
        for var in self.mat_vars:
            if var == 'Botlev':
                self.nc_add_swan_var(var, np.float32, 'node', 'depth', 'Bathymetry / bottom level',
                                     'm', np.array(self.mat_get_values(var)))

        # Variables with time steps
        direc = []
        hsig = []
        period = []

        for data in self.mat_times:
            for var in self.mat_vars:
                nom = '%s_%s' % (var, data.strftime('%Y%m%d_%H%M%S'))
                if var == 'Dir':
                    direc.append(np.array(self.mat_get_values(nom)))
                    self.nc_add_swan_var('Dir', np.float32, ('time', 'node'),
                                         'sea_surface_wave_from_direction', 'Mean wave direction',
                                         'degrees', direc[:])
                if var == 'Hsig':
                    hsig.append(np.array(self.mat_get_values(nom)))
                    self.nc_add_swan_var('Hsig', np.float32, ('time', 'node'),
                                         'sea_surface_wave_significant_height', 'Significant wave height',
                                         'm', hsig[:])
                if var == 'Period':
                    period.append(np.array(self.mat_get_values(nom)))
                    self.nc_add_swan_var('Period', np.float32, ('time', 'node'),
                                         'sea_surface_wave_mean_period', 'Mean absolute wave period',
                                         'seconds', period[:])
        logging.info("Data saved successfully on %s.", self.fitxer_desti)

    def __init__(self, fitxer_malla, fitxer_mat, fitxer_desti):
        logging.basicConfig(format='%(asctime)s %(levelname)-4s %(message)s',
                            level=logging.INFO,
                            datefmt='%Y-%m-%d %H:%M:%S')
        self.lons = self.lats = []
        self.cares = np.empty([0, 3], dtype=int)
        self.numcares = 0
        self.fitxer_desti = fitxer_desti
        self.fit_mat = fitxer_mat
        self.fit_nc = None
        self.variables = None
        self.mat_vars = None
        self.mat_times = None
        self.read_grid(fitxer_malla)
        self.nc_create_file()
        self.nc_write_mesh_data()
        self.nc_write_vars()


if __name__ == '__main__':
    Mat2Nc(sys.argv[1], sys.argv[2], sys.argv[3])

# How to build SWAN on MacOS

## Step 1. Install compilers and building tools
SWAN is a program written in Fortran, so we're going to install the GNU Fortran compiler and some building tools as they are Open Source.

Download the GNU Fortran compiler from https://gcc.gnu.org/wiki/GFortranBinaries#MacOS. After installing, check if the install was good by typing `gfortran --version` on the console. You should get something similar to this:

```bash
GNU Fortran (GCC) 11.1.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

After installing, we need to set the GNU Fortran compiler as the default compiler:
```bash
export FC=gfortran
```

## Step 2. Download the source code
Use `wget` command to download SWAN:
```bash
wget http://swanmodel.sourceforge.net/download/zip/swan4131.tar.gz
```
We need to extract the files before building. Most common procedure is to use the `tar` command with `-x`, `-z` and `-f` filters:
```bash
tar -xzf swan4131.tar.gz
```
Now all files were extracted to a new folder called `swan4131`. Let's dig into it:
```bash
cd swan4131
```


## Step 3. Build
Here's the point where the procedure diverges depending on which mode do you want to run SWAN:

* **Serial**: for computers only using one processor (like your PC)
* **Parallel, shared (OMP)**: for shared memory systems (like your PC, using multiple processors)

**Follow just one of the following (A or B) instructions!**

### A - Serial mode
Configure SWAN for Unix systems by executing the `switch.pl` Perl script:
```bash
perl switch.pl -unix *.ftn *.ftn90
```
Next, generate a config file based on your system:
```bash
make config
```

For the GNU Fortran compiler (starting on versions >=10), an additional flag is needed to build because some errors may and block the compilation. As a quick fix until someone fixes the source code, this flag converts those errors to warnings and the build is done without problems. To add those flags, modifiy the `macros.inc` file and modify this line:
```
FLAGS_SER = -fallow-argument-mismatch
```

Ready to build SWAN. To do it in serial mode, type:
```bash
make ser
```

### B - Parallel, shared mode (OMP)
Configure SWAN for Unix systems by executing the `switch.pl` Perl script:
```bash
perl switch.pl -unix *.ftn *.ftn90
```
Next, generate a config file based on your system:
```bash
make config
```
For the GNU Fortran compiler (starting on versions >=10), an additional flag is needed to build because some errors may and block the compilation. As a quick fix until someone fixes the source code, this flag converts those errors to warnings and the build is done without problems. To add those flags, modifiy the `macros.inc` file and modify this line:
```
FLAGS_OMP = -fopenmp -fallow-argument-mismatch
```
Ready to build SWAN. To do it in parallel-shared mode, type:
```bash
make omp
```

## Step 4. Finish
If no errors were shown during the building process, we've successfully built SWAN.

The built files need executable permissions. Let's fix them with `chmod +x` command:
```bash
chmod +x swanrun swan.exe
```
I like to have those files on my HOME folder (e.g. `/home/james`). Copy them with `cp` and move to it by `cd`:
```bash
cp -r 'swan.exe' 'swanrun' ${HOME}
cd ${HOME}
```
Once SWAN is built, we don't need the source files anymore. Clean up both the `.tar.gz` file and the `swan4131` folder:
```bash
rm -rf *.tar.gz swan4131
```

That's it. Now, you can run SWAN by calling `swanrun -input [filename]`.